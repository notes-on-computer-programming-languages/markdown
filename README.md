---
title: markdown
language: yaml
---
# markdown

Lightweight markup language with plain text formatting syntax. https://en.m.wikipedia.org/wiki/Markdown

# Table of contents
[[_TOC_]]

# Some Implementations (See also Wikipedia)
* pandoc
* mmark
  * Haskell
    * [Stackage](https://www.stackage.org/package/mmark)
    * [mmark-cli](https://www.stackage.org/package/mmark-cli)
    * [Repology](https://repology.org/project/haskell:mmark-cli/versions)
  * Golang
    * [mmarkdown/mmark](https://github.com/mmarkdown/mmark)
* [Elixir](https://hex.pm/packages?search=markdown&page=1&sort=recent_downloads)
* Ocaml...
* PHP [![PHPPackages Rank](http://phppackages.org/p/erusev/parsedown/badge/rank.svg)](http://phppackages.org/p/erusev/parsedown)
  erusev/parsedown
* CommonMark specification
  * GitLab Flavored Markdown (GFM) inspirated by GitHub Flavored Markdown
  * https://github.com/commonmark (cmark)
* Kramdown (GitLab documentation website or GitLab’s main website)

# Extensions and regular features
Some extension or regular features may allow
* [*GitLab Docs > User Docs > GitLab Markdown*](https://docs.gitlab.com/ee/user/markdown.html)

## Colors
* `RGB(0%,100%,0%)`
* `RGBA(0%,100%,0%,0.3)`

## Math with KaTex
This math is inline $`a^2+b^2=c^2`$.

This is on a separate line

```math
a^2+b^2=c^2
```

## Images
### SVG
* The supported formats are .png, .jpg, .gif. You might be able to use some .svg files too, depending on its structure.
  * https://about.gitlab.com/handbook/engineering/technical-writing/markdown-guide/#images

### Directly embedded
* [*Is it possible to directly embed an image into a Markdown document?*
  ](https://superuser.com/questions/1199393/is-it-possible-to-directly-embed-an-image-into-a-markdown-document)
  2017-2019
  * ![Hello World](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAAAUCAAAAAAVAxSkAAABrUlEQVQ4y+3TPUvDQBgH8OdDOGa+oUMgk2MpdHIIgpSUiqC0OKirgxYX8QVFRQRpBRF8KShqLbgIYkUEteCgFVuqUEVxEIkvJFhae3m8S2KbSkcFBw9yHP88+eXucgH8kQZ/jSm4VDaIy9RKCpKac9NKgU4uEJNwhHhK3qvPBVO8rxRWmFXPF+NSM1KVMbwriAMwhDgVcrxeMZm85GR0PhvGJAAmyozJsbsxgNEir4iEjIK0SYqGd8sOR3rJAGN2BCEkOxhxMhpd8Mk0CXtZacxi1hr20mI/rzgnxayoidevcGuHXTC/q6QuYSMt1jC+gBIiMg12v2vb5NlklChiWnhmFZpwvxDGzuUzV8kOg+N8UUvNBp64vy9q3UN7gDXhwWLY2nMC3zRDibfsY7wjEkY79CdMZhrxSqqzxf4ZRPXwzWJirMicDa5KwiPeARygHXKNMQHEy3rMopDR20XNZGbJzUtrwDC/KshlLDWyqdmhxZzCsdYmf2fWZPoxCEDyfIvdtNQH0PRkH6Q51g8rFO3Qzxh2LbItcDCOpmuOsV7ntNaERe3v/lP/zO8yn4N+yNPrekmPAAAAAElFTkSuQmCC)

## Video
* http://www.w3schools.com/tags/tag_video.asp
* https://developer.mozilla.org/en-US/docs/Web/HTML/Element/video

## Diagrams and flowcharts
### Mermaid
* https://mermaid-js.github.io/mermaid

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```

```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "SubGraph 1 Flow"
  SubGraph1Flow(SubNode 1)
  SubGraph1Flow -- Choice1 --> DoChoice1
  SubGraph1Flow -- Choice2 --> DoChoice2
  end

  subgraph "Main Graph"
  Node1[Node 1] --> Node2[Node 2]
  Node2 --> SubGraph1[Jump to SubGraph1]
  SubGraph1 --> FinalThing[Final Thing]
end
```

## Emoji
* :star2:

## Data URLs
* [*Data URLs*](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URIs)
  * data:,Hello%2C%20World!
  * data:text/plain;base64,SGVsbG8sIFdvcmxkIQ==
  * data:text/html,%3Ch1%3EHello%2C%20World!%3C%2Fh1%3E
  * data:text/html,<script>alert('hi');</script>

## Adaptive screen and line warping, page width on narrow screens...

# [Edit this page](https://gitlab.com/notes-on-computer-programming-languages/markdown/edit/master/README.md)